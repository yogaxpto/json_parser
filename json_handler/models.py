from django.db.models import Model, Field, CharField, FloatField, IntegerField, ForeignKey, CASCADE
from . import static


class Device(Model):
    id: Field = CharField(max_length=static.uuid_length, primary_key=True)
    latitude: FloatField = FloatField()
    longitude: FloatField = FloatField()


class Location(Model):
    Source: ForeignKey = ForeignKey(Device, CASCADE)
    type: Field = CharField(max_length=static.uuid_length_type)
    value: IntegerField = IntegerField()

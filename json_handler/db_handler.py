from .models import Device, Location
from django.db import connection
import os


def json_parser_orm(collection: dict) -> None:
    for iterator in collection:
        json_device = collection[iterator]['device']
        new_device: Device = Device.objects.create(id=json_device['id'],
                                                   latitude=json_device['latitude'],
                                                   longitude=json_device['longitude'],
                                                   )
        new_location: Location = Location.objects.bulk_create([Location(Source=new_device,
                                                                        type=json_device['location'][str(iterator_2)]
                                                                            ['type'],
                                                                        value=json_device['location'][str(iterator_2)]
                                                                            ['value']) for iterator_2 in
                                                               range(len(json_device['location']))])
        new_device.save()


def json_parser(collection: dict):
    sql_query = ""
    for iterator in collection:
        json_device = collection[iterator]['device']
        sql_query += 'INSERT INTO json_handler_device(id,latitude,longitude) VALUES(' + \
                     ','.join(
                         ['\'' + json_device['id'] + '\'', str(json_device['latitude']),
                          str(json_device['longitude'])]) + ');' + os.linesep
        for iterator_2 in json_device['location']:
            json_location = json_device['location'][iterator_2]
            sql_query += 'INSERT INTO json_handler_location(Source_id, type, value) VALUES (' + \
                         ','.join(['\'' + json_device['id'] + '\'', ('\'' + json_location['type'] + '\''),
                                   str(json_location['value'])]) + ');' + os.linesep
    with connection.cursor() as cursor:
        cursor.executescript(sql_query)


def clean_tables():
    with connection.cursor() as cursor:
        cursor.execute('DELETE FROM json_handler_location;')
        cursor.execute('DELETE FROM json_handler_device;')

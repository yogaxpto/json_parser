from django.http import HttpResponse, HttpResponseBadRequest, HttpRequest, JsonResponse
from django.core import serializers
import requests
from .utils import *
from . import static, db_handler
import time


def valid_request(request: HttpRequest) -> bool:
    # TODO: validation
    return True


def json_handler(request: HttpRequest, number_ids: int, average_locations: int) -> HttpResponse:
    if valid_request(request):
        start = time.time()
        parse(request, number_ids, average_locations)
        return HttpResponse('Time elapsed:' + str(round(time.time() - start, 4)) + 's', status=200)
    return HttpResponse(status=400)


def parse(request: HttpRequest, number_ids: int, average_locations: int) -> None:
    url = 'http://localhost:' + str(default_port) + '/createjson/' + str(number_ids) + '/' + str(average_locations)
    try:
        response = requests.get(url).json()
        db_handler.json_parser_orm(response)
    except Exception as e:
        # TODO: requests and db interactions should have their own Exception handles
        print(e)
        pass


def json_create(request: HttpRequest, number_ids: int, average_locations: int) -> JsonResponse:
    if number_ids > 9999:
        return JsonResponse(custom_random_json(number_ids, average_locations),
                            content_type='application/force-download')
    return JsonResponse(custom_random_json(number_ids, average_locations))


def cleandb(request: HttpRequest) -> HttpResponse:
    db_handler.clean_tables()
    return HttpResponse(status=204)

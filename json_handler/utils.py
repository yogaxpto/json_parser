import random, string
from .static import *



class Device:
    def __init__(self, average):
        self.id: string = random_str(uuid_length)
        self.latitude: float = random_latitude()
        self.longitude: float = random_longitude()
        self.locations: [Location] = [Location() for _ in range(round(random.uniform(1, average + 1)))]

    def to_dict(self) -> dict:
        result = dict(id=self.id,
                      latitude=self.latitude,
                      longitude=self.longitude,
                      location={}
                      )
        for i in range(len(self.locations)):
            result['location'][i] = self.locations[i].to_dict()
        return result


class Location:
    def __init__(self):
        self.type: str = random_str(uuid_length_type)
        self.value: int = random_decimal()

    def to_dict(self):
        return dict(type=self.type,
                    value=self.value)


def custom_random_json(number_ids: int, average_number_locations: float) -> dict:
    result: dict = {}
    for x in range(number_ids):
        result[x] = {'device': Device(average_number_locations).to_dict()}
    return result


def random_str(size: int) -> str:
    return ''.join([random.choice(string.ascii_letters + string.digits) for _ in range(size)])


def random_latitude() -> float:
    return round(random.uniform(-85, 85), 6)


def random_longitude() -> float:
    return round(random.uniform(-180, 180), 6)


def random_decimal() -> int:
    return random.randint(0, 9)
